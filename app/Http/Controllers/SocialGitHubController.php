<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;
use Socialite;
use App\User;

class SocialGitHubController extends Controller
{

	//Для работы этого нужно поставить сервер https://developer.github.com/webhooks/configuring/
	//И добавить ссылку в логин view: <a href="/auth/github">Login with GitHub</a>
	public function redirectToProvider(){
		return Socialite::driver('github')->redirect();
	}

	public function handleProviderCallback(){
		$user = Socialite::driver('github')->user();
		$existingUser = User::where('provider_id', $user->getId())->first();

		if ($existingUser) {
			Auth::login($existingUser);
			dd($existingUser);
		} else{
			$newUser = new User();
			$newUser->email = $user->getEmail();
			$newUser->provider_id = $user->getId();
			$newUser->handle_github = $user->getNickname();
			$newUser->password = bcrypt(uniqid());

			dump($newUser);
			$newUser->save();
			Auth::login($newUser);
		}
		flash('Successfully authenticated using GitHub');
		return redirect('/');
	}
}
