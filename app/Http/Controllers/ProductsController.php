<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Shop;
use Intervention\Image\Facades\Image as Image;
class ProductsController extends Controller
{
const PAGIN = 10;
    public function index()
    {
	    $shops = Shop::all();
        $products = Product::where('shop_id', $shops->first()->id)->paginate(self::PAGIN);
        return view('products.index', compact('products', 'shops'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
	    $file = $request->file('img'); //?
	    $extension = $file->getClientOriginalName();
	    $fileName = time().'_'.$extension;
	    $resizedImage = Image::make($file)->resize(640,480);
	    $resizedImage->save('images/products/'.$fileName);

	    $product=new Product ($request->all());
	    $product->img=$fileName;
	    $product->save();

	    flash('Product created')->success();
	    return redirect('/home');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
		$product = Product::findOrFail($id);
	    return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
	    //$product = Product::findOrFail($id);
	    $file = $request->file('img'); //?
	    $extension = $file->getClientOriginalName();
	    $fileName = time().'_'.$extension;
	    $resizedImage = Image::make($file)->resize(640,480);
	    $resizedImage->save('images/products/'.$fileName);

	    Product::where('Id', $id)->update([
		    'name' =>  $request->input('name'),
		    'description' => $request->input('description'),
	        'shop_id' => $request->input('shop_id'),
	        'price' => $request->input('price'),
	        'img'=>$fileName
	    ]);
	    return redirect('/products');

    }
	//$value = str_limit('The PHP framework for web artisans.', 7);
    //поиск по нескольким параметрам для дататабл на беке, через несколько where?

    public function destroy($id)
    {
	    Product::where('Id', $id)->delete();
	    return redirect('/products');
    }

	public function delete($id)
	{
		$product = Product::findOrFail($id);
		return view('products.delete', compact('product'));
	}

	//Метод для просмотра продуктов для начального магазина
	public function product($id=null) {
		if($id==null) {
			$min_id = min(Product::select('shop_id')->get()->toArray());
			$products = Product::orderby('id')
				->where('shop_id', '=', $min_id['shop_id'])
				->get();
		} else {
			$products = Product::orderby('id')->where('shop_id', '=', $id)
				->get();
		}
		return response()->json($products);
	}

	public function shop()
	{
		$shops = Shop::orderby('id')->get();
		return response()->json($shops);
	}
	//В контроллере выбирать данные по нескольким инпутам, а там передавать их по jquery тип в переменную по имени и потом сюда массивом в аякс.
	//Метод для просмотра продуктов по переключению магазинов
	public function viewProduct($id) {
		$products = Product::orderby('id')->where('shop_id', '=', $id)
			->get();
		return response()->json($products);
	}
}
