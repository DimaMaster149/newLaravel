<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Http\Response;
class TableController extends Controller
{
	//
	public function tableProduct() {
		$products = Product::orderby('id')->get();
		return view('products.table')->with(['products'=>$products]);
	}

	public function tableSearch(Request $request) {
		$q = Product::query();

		if ($request->name != '') {
			$q->where('name', 'like', '%'.$request->name.'%');
		}
		if ($request->price != '') {
			$q->where('price', 'like', '%'.$request->price.'%');
		}

		$products = $q->get();
		return Response($products);
	}


}

