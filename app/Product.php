<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'name', 'description', 'shop_id', 'price', 'img'
	];
	public function shop()
	{
		return $this->belongsTo(Shop::class);
	}
}
