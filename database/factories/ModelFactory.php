<?php

use Faker\Generator as Faker;
use App\Shop;
use App\Product;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Shop::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'address' => $faker->unique()->streetAddress,
	];
});

$factory->define(App\Product::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'shop_id' => App\Shop::all()->random()->id,
		'description' => $faker->paragraph(2),
		'price' => $faker->numberBetween(1, 100),
		'img' => $faker->image(public_path().'/images/products',200, 200, 'cats', false),
	];
});