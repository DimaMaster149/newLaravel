<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('products', 'ProductsController');

//Пути для отдельных методов
//Продукты, магазины и таблица продуктов
Route::get('/goods/products/{shop_id?}', 'ProductsController@product')->name('product');
Route::get('/goods/shops', 'ProductsController@shop')->name('shop');
Route::get('/table', 'TableController@tableProduct')->name('tableProduct');
//Поиск по таблице
Route::post('/table/search', 'TableController@tableSearch')->name('tableSearch');
//через дополнительную форму
Route::get('/products/{id?}/delete', 'ProductsController@delete');

//ГитХаб авторизация
Route::get('auth/github', 'SocialGitHubController@redirectToProvider');
Route::get('auth/github/callback', 'SocialGitHubController@handleProviderCallback');