@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Delete</h1>
        <form method="POST" action="/products/{{$product->Id}}" enctype='multipart/form-data'>
            {{method_field('DELETE')}}
            <div class="form-group">
                <label for="name"> Enter product name </label>
                <input type="text" class="form-control" name="name" value="{{$product->name}}">
            </div>
            <button type="submit" class="btn btn-danger">Delete</button>
            {{csrf_field() }}
        </form>
    </div>

@endsection