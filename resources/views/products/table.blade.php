@extends('layouts.app')

@section('content')

    <form method="POST" action="/table/search" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="name"> Enter product name </label>
            <input type="text" class="form-control" name="name" value="">
        </div>
        <div class="form-group">
            <label for="price"> Enter product price </label>
            <input type="number" class="form-control" name="price" value="" >
        </div>
        {{csrf_field()}}
        <button type="button" class="btn btn-success search">Search</button>
    </form>

    <table id="myTable" class="display">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Shop id</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{$product->Id}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->shop_id}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('scripts')
<script src="/js/app.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#myTable').DataTable();

        $('.search').on('click', function(){
            var name = $( "input[name='name']" ).val();
            var price = $( "input[name='price']" ).val();
            table.clear().draw();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/table/search',
                data: {name:name, price:price},
                type: 'POST',
                success: function(data) {
                    if(!$.isEmptyObject(data)) {
                        $.each(data, function(index, value){
                            table.row.add( [
                                value.Id ,
                                value.name,
                                value.description,
                                value.price,
                                value.shop_id
                            ]).draw();
                        });
                    }

                },
                error: function(e){
                    console.log(e);
                }
            });
        });



       /* //const $el = $(e.target),
        //$form = $el.closest('.js-item-form');
        var name = $( "input[name='name']" ).val();
        var price = $( "input[name='price']" ).val();
        $('#myTable').DataTable( {
            processing:true,
            serverSide: true,
            ajax: ({
                url: '/table/search',
                data: {name:name, price:price},
                dataType: 'json',
                //data: $form.serialize(),
                success: function(response){
                    console.log(response);
                }
            }),
            columns: [
                { data: 'Id' },
                { data: 'name' },
                { data: 'description' },
                { data: 'price' },
                { data: 'shop_id' }
            ]
        });*/
    });


</script>
@endsection


