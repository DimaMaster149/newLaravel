@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class='panel panel-info'>

                    <div class="panel-heading text-center">
                        <h4>Shops</h4>
                    </div>
                    @foreach($shops as $shop)
                        <a href="#" class="sidebar" data-id="{{$shop->id}}">{{$shop->name}}</a>
                        <br>
                    @endforeach
                </div>
            </div>
            <div class="magazine">
                @forelse($products as $product)
                    <div class="col-md-4 card">
                        <h3>Product name:  <strong>{{$product->name}}</strong> </h3>
                        <p>Description: <br>{{$product->description}}</p>
                        <p>Product price is {{$product->price}}$</p>
                        <p>Shop id: {{$product->shop_id}}</p>
                        <img src = "{{asset("/images/products/" . $product->img)}}" alt="">
                        <a href="/products/{{$product->Id}}/edit" class="btn btn-success pull-left">Edit</a>
                        <a href="/products/{{$product->Id}}/delete" class="btn btn-danger pull-right">Delete</a>
                        </form>

                    </div>
                @empty
                    <h2>No products available</h2>
                @endforelse
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {{$products->links()}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready()
    {
        console.log('ready');
        $('.sidebar').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.get("/goods/products/"+id, function( data ) {
                var text;
                $('.card').remove();
                $.each(data, function(index, item){
                        text += `
                     <div class="col-md-4 card">
                    <h3>Product name:  <strong>`+ item["name"] +`</strong> </h3>
                    <p>Description: <br>`+ item["description"] +`</p>
                    <p>Product price is` + item["price"] +`</p>
                    <p>Shop id:` + item["shop_id"] +`</p>
                    <img src = "/images/products/` + item["img"] +`" alt="">
                    <a href="/products/`+ item['Id'] +`/edit" class="btn btn-success pull-left">Edit</a>
                    <a href="/products/`+ item['Id'] +`/delete" class="btn btn-danger pull-right">Delete</a>
                    </form>
                    </div>`
                })
                $('.magazine').html(text);

                //$( ".result" ).html( data );
                //alert( "Load was performed." );
            });
        })

    }
    //this.$http.get("/goods/products/"+id).then(function(resp){
</script>
@endsection
<style>
    .card{
        font-size:18px;
        height:680px;
    }
    img{
        width:100%;
        height: 250px;
        padding:20px;
    }
    a{
        width:100px;
        font-size:16px;
    }
</style>