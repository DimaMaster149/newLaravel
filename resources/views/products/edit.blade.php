@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit</h1>
        <form method="POST" action="/products/{{$product->Id}}" enctype='multipart/form-data'>
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="name"> Enter product name </label>
                <input type="text" class="form-control" name="name" value="{{$product->name}}">
            </div>
            <div class="form-group">
                <label for="description"> Enter product description </label>
                <textarea type="text" class="form-control" name="description">{{$product->description}} </textarea>
            </div>
            <div class="form-group">
                <label for="shop_id"> Enter shop id </label>
                <input type="number" class="form-control" name="shop_id" value="{{$product->shop_id}}" placeholder="From 1 to 10">
            </div>
            <div class="form-group">
                <label for="price"> Enter product price </label>
                <input type="number" class="form-control" name="price" value="{{$product->price}}" >
            </div>
            <div class="form-group">
                <label for="img"> Enter file </label>
                <input type="file" class="form-control" name="img" value="" >
            </div>
            <button type="submit" class="btn btn-success">Edit product</button>
            {{csrf_field() }}
        </form>
    </div>

@endsection